import java.util.Scanner;

public class SalaryCalculation {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        int [] days = new int[7];
        for (int i = 0; i < 7; i++) {
            days[i] = scan.nextInt();
        }

        double salary = 0;
        int noOfHours = 0;
        for (int i = 0; i < 7; i++) {
            salary += (days[i] * 100);
            if (days[i] > 8) {
                salary += (days[i] - 8) * 15;
            }
            noOfHours += days[i];
        }
        if(noOfHours >40)
        {
            salary = salary +(noOfHours -40)*25;
        }
        else
        {
            salary +=(days[0]*100)*0.50;
            salary +=(days[6]*100)*0.25;
        }

        System.out.println(salary);//total salary

    }
}

