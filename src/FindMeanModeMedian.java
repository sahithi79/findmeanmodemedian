import java.util.*;
public class FindMeanModeMedian {
    public static void main(String[] args)
    {

        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        int[] array = new int[number];
        for (int i = 0; i < number; i++)
        {
            array[i] = sc.nextInt();
        }
        double mean=calculatingMean(array);
        double median=calculatingMedian(array);
        double mode=calculatingMode(array);
        System.out.println(mean);
        System.out.println(median);
        System.out.println(mode);
    }
    public static double calculatingMean(int[] array)
    {
        int number=array.length;

        int meanSum = 0;
        for (int i = 0; i < number; i++) {
            meanSum += array[i];
        }
        return ((double) meanSum / (double) number);
    }
    public static double calculatingMedian(int[] array)
    {
        double median;
        int number=array.length;
        Arrays.sort(array);
        if (number % 2 != 0) {
            median=array[number / 2];
            return median;

        }

        median=((array[(number - 1) / 2])+array[number / 2])/2.0;
        return median;

    }
    public static double calculatingMode(int[] arr)
    {
        int max_count = 1, res = arr[0], cur_count = 1;
        for (int i = 1; i < arr.length; i++)
        {
            if (arr[i] == arr[i - 1])
                cur_count++;
            else
            {
                if (cur_count > max_count)
                {
                    max_count = cur_count;
                    res = arr[i - 1];
                }
                cur_count = 1;
            }
        }
        // If last element is most frequent
        if (cur_count > max_count)
        {
            max_count = cur_count;
            res = arr[arr.length - 1];
        }

        return res;
    }

}
